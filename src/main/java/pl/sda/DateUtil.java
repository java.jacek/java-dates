package pl.sda;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class DateUtil {
    public static long between(String dateInFuture) {
        System.out.println("Enter: DateUtil.between(" + dateInFuture + ")");

        LocalDate localDate = LocalDate.parse(dateInFuture, DateTimeFormatter.ISO_DATE);
        LocalDate localDateNow = LocalDate.now();

        long between = ChronoUnit.DAYS.between(localDateNow, localDate);
        System.out.println("Exit: DateUtil.between("
                + dateInFuture + ") = " + between);

        return between;
    }
}
